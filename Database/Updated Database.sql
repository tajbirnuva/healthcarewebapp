USE [master]
GO
/****** Object:  Database [HealthCare]    Script Date: 2/5/2021 1:38:20 AM ******/
CREATE DATABASE [HealthCare]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'HealthCare', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\HealthCare.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'HealthCare_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\HealthCare_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [HealthCare] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [HealthCare].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [HealthCare] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [HealthCare] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [HealthCare] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [HealthCare] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [HealthCare] SET ARITHABORT OFF 
GO
ALTER DATABASE [HealthCare] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [HealthCare] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [HealthCare] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [HealthCare] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [HealthCare] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [HealthCare] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [HealthCare] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [HealthCare] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [HealthCare] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [HealthCare] SET  ENABLE_BROKER 
GO
ALTER DATABASE [HealthCare] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [HealthCare] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [HealthCare] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [HealthCare] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [HealthCare] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [HealthCare] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [HealthCare] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [HealthCare] SET RECOVERY FULL 
GO
ALTER DATABASE [HealthCare] SET  MULTI_USER 
GO
ALTER DATABASE [HealthCare] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [HealthCare] SET DB_CHAINING OFF 
GO
ALTER DATABASE [HealthCare] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [HealthCare] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [HealthCare] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [HealthCare] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'HealthCare', N'ON'
GO
ALTER DATABASE [HealthCare] SET QUERY_STORE = OFF
GO
USE [HealthCare]
GO
/****** Object:  Table [dbo].[DiagnosticCenter]    Script Date: 2/5/2021 1:38:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DiagnosticCenter](
	[CenterId] [int] NOT NULL,
	[CenterName] [varchar](150) NOT NULL,
	[Location] [varchar](150) NOT NULL,
	[City] [varchar](150) NOT NULL,
	[Email] [varchar](150) NOT NULL,
 CONSTRAINT [PK_DiagnosticCenter] PRIMARY KEY CLUSTERED 
(
	[CenterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DoctorDesignation]    Script Date: 2/5/2021 1:38:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoctorDesignation](
	[DesignationId] [int] IDENTITY(1,1) NOT NULL,
	[DesignationName] [varchar](150) NOT NULL,
 CONSTRAINT [PK_DoctorDesignation] PRIMARY KEY CLUSTERED 
(
	[DesignationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MedicalTest]    Script Date: 2/5/2021 1:38:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalTest](
	[CenterId] [int] NOT NULL,
	[TestName] [varchar](150) NOT NULL,
	[TestPrice] [varchar](150) NOT NULL,
	[TestTime] [varchar](150) NOT NULL,
	[TestPrerequisite] [varchar](150) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PatientData]    Script Date: 2/5/2021 1:38:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PatientData](
	[UserId] [int] NOT NULL,
	[Date] [varchar](50) NOT NULL,
	[Time] [varchar](50) NOT NULL,
	[BloodPressure] [varchar](50) NOT NULL,
	[Diabetes] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 2/5/2021 1:38:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[PatientId] [int] NOT NULL,
	[PatientName] [varchar](max) NOT NULL,
	[DoctorId] [int] NOT NULL,
	[DoctorName] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Prescription]    Script Date: 2/5/2021 1:38:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prescription](
	[ImageId] [int] IDENTITY(1,1) NOT NULL,
	[Image] [varbinary](max) NOT NULL,
	[PrescribedBy] [varchar](max) NOT NULL,
	[Designation] [varchar](max) NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_Prescription] PRIMARY KEY CLUSTERED 
(
	[ImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Report]    Script Date: 2/5/2021 1:38:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Report](
	[ImageId] [int] IDENTITY(1,1) NOT NULL,
	[ReportName] [varchar](max) NOT NULL,
	[PrescribedBy] [varchar](max) NOT NULL,
	[Designation] [varchar](max) NOT NULL,
	[Image] [varbinary](max) NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED 
(
	[ImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 2/5/2021 1:38:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NOT NULL,
	[Password] [varchar](max) NOT NULL,
	[UserType] [varchar](50) NOT NULL,
	[Email] [varchar](max) NOT NULL,
	[Mobile] [varchar](100) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [HealthCare] SET  READ_WRITE 
GO
