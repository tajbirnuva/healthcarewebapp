﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HealthCareWebApp.BLL;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp
{
    public partial class DoctorUserHomeUI : System.Web.UI.Page
    {
        UserManager aUserManager = new UserManager();
        Users aUser = new Users();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                string email = Session["User"].ToString();
                aUser = aUserManager.GetUsers(email);
                if (!IsPostBack)
                {
                    patientDropDownList.DataSource = aUserManager.GetAllPermittedPatient(aUser.UserId);
                    patientDropDownList.DataTextField = "PatientName";
                    patientDropDownList.DataValueField = "PatientId";
                    patientDropDownList.DataBind();
                    patientDropDownList.Items.Insert(0, "Select Your Patient");

                }


            }
            else
            {
                Response.Redirect("loginUI.aspx");
            }
        }


        protected void patientDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (patientDropDownList.SelectedValue == "Select Your Patient")
            {
                saveLavel.Text = "Select Patient's Name";
                mobileTextbox.Text = String.Empty;

            }
            else
            {
                int patientId = Convert.ToInt32(patientDropDownList.SelectedValue);

                Users aUsers = aUserManager.GetUsersById(patientId);
                mobileTextbox.Text = aUsers.Mobile.ToString();

                saveLavel.ForeColor = System.Drawing.Color.ForestGreen;
                saveLavel.Text = "Check Mobile Number To Sure Right Patient";
            }
        }



        protected void showButton_Click(object sender, EventArgs e)
        {
            if (patientDropDownList.SelectedValue == "Select Your Patient")
            {
                saveLavel.Text = "Select Patient's Name";
                mobileTextbox.Text = String.Empty;
            }
            else
            {
                Session["PatientId"] = Convert.ToInt32(patientDropDownList.SelectedValue);
                Response.Redirect("PatientDetailsUI.aspx");
            }

        }
    }
}