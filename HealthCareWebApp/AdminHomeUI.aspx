﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminHomeUI.aspx.cs" Inherits="HealthCareWebApp.AdminHomeUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Account</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/tooplate-style.css">


</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>



<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="loginUI.aspx" class="navbar-brand">P M <i class="fa fa-h-square"></i> System</a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="AdminHomeUI.aspx" class="smoothScroll">Client</a></li>
                <li><a href="AdminDiagonisticCenterUI.aspx" class="smoothScroll">Diagnostic-Center</a></li>
             

                <li class="appointment-btn"><a href="loginUI.aspx">Log Out</a></li>
            </ul>
        </div>
    </div>
</section>




    
      <form id="form1" runat="server">
        <section id="appointment" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">

           <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h2>Client Sign Up</h2>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                  
                                  <div class="col-md-12 col-sm-12">
                                      <label for="email">Name</label>
                                      <asp:TextBox ID="nameTextBox" class="form-control" placeholder="Your Full Name" runat="server"></asp:TextBox>
                                      <label for="email">Email</label>
                                      <asp:TextBox ID="emailTextBox" class="form-control" placeholder="Your Email" runat="server"></asp:TextBox>
                                       

                                      <label for="telephone">Password</label>
                                      <asp:TextBox ID="passTextBox" class="form-control" placeholder="Password" TextMode="Password"  runat="server"></asp:TextBox>
                                       
                                     
                                  </div>
                                  
                                  <div class="col-md-6 col-sm-6">
                                      <label for="select">Select User Type</label> 
                                      <asp:DropDownList ID="usertypeDropDownList" class="form-control" runat="server">
                                          <asp:ListItem>Select User Type</asp:ListItem>
                                          <asp:ListItem>Diagnostic</asp:ListItem>
                                          <asp:ListItem>Admin</asp:ListItem>
                                      </asp:DropDownList>  
                                  </div>
                                      
                                  <div class="col-md-6 col-sm-6">
                                      
                                      <label for="telephone">Mobile</label>
                                      <asp:TextBox ID="mobileTextBox" class="form-control" placeholder="Mobile Number"  runat="server"></asp:TextBox>

                                  </div>
                                  

                                  <div class="col-md-6 col-sm-6">
                                      <asp:Label ID="signupLabel" runat="server" Text=""></asp:Label>
                                  </div>
                                  <div class="col-md-6 col-sm-6">
                                      <asp:Button ID="signupButton" class="form-control" runat="server" Text="Sign Up" OnClick="signupButton_Click" />
                                     
                                  </div>

                              </div>

        </div>

               </div>
          </div>

     </section>
      </form>

<!-- SCRIPTS -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>