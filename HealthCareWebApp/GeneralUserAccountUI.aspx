﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralUserAccountUI.aspx.cs" Inherits="HealthCareWebApp.GeneralUserAccountUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Account</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/tooplate-style.css">


</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>



<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="loginUI.aspx" class="navbar-brand">P M <i class="fa fa-h-square"></i> System</a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="GeneralUserHomeUI.aspx" class="smoothScroll">Home</a></li>
                <li><a href="GeneralUserPrescriptionUI.aspx" class="smoothScroll">Prescription</a></li>
                <li><a href="GeneralUserReportUI.aspx" class="smoothScroll">Reports</a></li>

                <li class="appointment-btn"><a href="loginUI.aspx">Log Out</a></li>
            </ul>
        </div>
    </div>
</section>




    
      <form id="form1" runat="server">
        <section id="appointment" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h4>Doctor's Access Permission</h4>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                  
                                  <div class="col-md-6 col-sm-6">
                                      <label for="name">Doctor</label>
                                      <asp:DropDownList ID="doctorDropDownList" class="form-control" runat="server" OnSelectedIndexChanged="doctorDropDownList_SelectedIndexChanged"  AutoPostBack="True">
                                         
                                      </asp:DropDownList>  

                                  </div>

                                  <div class="col-md-6 col-sm-6">
                                      <label for="name">Mobile Number</label>
                                      <asp:TextBox ID="mobileTextbox" class="form-control" placeholder="Your Doctor's Mobile Number" runat="server" ReadOnly="True"></asp:TextBox>

                                  </div>
                                  

                                  <div class="col-md-6 col-sm-6">
                                      <asp:Label ID="saveLavel" runat="server" Text=""></asp:Label>
                                  </div>
                                

                                  <div class="col-md-6 col-sm-6">
                                     
                                      <asp:Button ID="permissionButton" class="form-control" runat="server" Text="Give Permission" OnClick="permissionButton_Click" />
                                     
                                  </div>

                                
                                  

                              </div>

        </div>
        
          <div class="col-md-6 col-sm-6">
              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                  <h3>Selected Doctors List</h3>
              </div>

                         <!-- CONTACT FORM HERE -->
              <div class="wow fadeInUp" data-wow-delay="0.8s">

                  <div class="col-md-12 col-sm-12">
                      <asp:GridView ID="selectedDoctortGridView" runat="server" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal" Width="100%" AutoGenerateColumns="False">
                          <FooterStyle BackColor="White" ForeColor="#333333" />
                          <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                          <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                          <RowStyle BackColor="White" ForeColor="#333333" />
                          <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                          <SortedAscendingCellStyle BackColor="#F7F7F7" />
                          <SortedAscendingHeaderStyle BackColor="#487575" />
                          <SortedDescendingCellStyle BackColor="#E5E5E5" />
                          <SortedDescendingHeaderStyle BackColor="#275353" />


                          <Columns>
                
                              <asp:TemplateField HeaderText="Doctor's Name">
                                  <ItemTemplate>
                                      <asp:HiddenField ID="idHiddenField" runat="server" Value='<%#Eval("DoctorId")%>'/>
                                      <asp:Label runat="server" Text='<%#Eval("DoctorName")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                              

                           
                              <asp:TemplateField HeaderText="Action">
                                  <ItemTemplate>
                                     
                                      <asp:LinkButton ID="deleteLinkButton" runat="server" OnClick="deleteLinkButton_OnClick"  OnClientClick="return confirm('Delete Doctor From Permission List?')" >Delete From List</asp:LinkButton>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                             

                          </Columns>
                          

                      </asp:GridView>
                  </div>
                            
                        </div>   
                    </div>

               </div>
          </div>

     </section>
      </form>

<!-- SCRIPTS -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>