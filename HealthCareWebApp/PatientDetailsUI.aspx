﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PatientDetailsUI.aspx.cs" Inherits="HealthCareWebApp.PatientDetailsUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Patient Details</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/tooplate-style.css">


</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>



<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="loginUI.aspx" class="navbar-brand">P M <i class="fa fa-h-square"></i> System</a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="DoctorUserHomeUI.aspx" class="smoothScroll">Home</a></li>

                <li class="appointment-btn"><a href="">Account</a></li>
            </ul>
        </div>
    </div>
</section>




    
      <form id="form1" runat="server">
        <section id="info" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h3>Patient</h3>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                  
                                  <div class="col-md-3 col-sm-3">
                                      <label for="name">Patient</label>
                                      <asp:TextBox ID="patientNameTextBox" class="form-control" runat="server" ReadOnly="True"></asp:TextBox>


                                  </div>

                                  <div class="col-md-3 col-sm-3">
                                      <label for="name">Mobile Number</label>
                                      <asp:TextBox ID="mobileTextbox" class="form-control"  runat="server" ReadOnly="True"></asp:TextBox>

                                  </div>
                                  
                                  <div class="col-md-6 col-sm-6"></div>

                              </div>

        </div>
            
       
        

               <div class="col-md-12 col-sm-12">
                   
                   <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                      
                   </div>

                         <!-- CONTACT FORM HERE -->
              <div class="wow fadeInUp" data-wow-delay="0.8s">

                  <div class="col-md-6 col-sm-6">
                      <label for="name">Patient Regular History</label>
                      <asp:GridView ID="patientHistoryGridView" runat="server" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal" Width="100%" AutoGenerateColumns="False">
                          <FooterStyle BackColor="White" ForeColor="#333333" />
                          <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                          <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                          <RowStyle BackColor="White" ForeColor="#333333" />
                          <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                          <SortedAscendingCellStyle BackColor="#F7F7F7" />
                          <SortedAscendingHeaderStyle BackColor="#487575" />
                          <SortedDescendingCellStyle BackColor="#E5E5E5" />
                          <SortedDescendingHeaderStyle BackColor="#275353" />


                          <Columns>
                
                              <asp:TemplateField HeaderText="Date">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("Date")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Time">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("Time")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Blood Pressure">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("BloodPressure")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Diabetes">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("Diabetes")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>

                          </Columns>
                          

                      </asp:GridView>
                  </div>
                            
                  <div class="col-md-6 col-sm-6"></div>
                        </div>   
                    </div>
        
        
            
            <!-- Prescription -->
              <div class="col-md-12 col-sm-12">
                  <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                      
                  </div>
                         <!-- CONTACT FORM HERE -->
              <div class="wow fadeInUp" data-wow-delay="0.8s">

                  <div class="col-md-6 col-sm-6">
                      <label for="name">Patient Prescritions</label>
                      <asp:GridView ID="prescriptionGridView" runat="server" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal" Width="100%" AutoGenerateColumns="False">
                          <FooterStyle BackColor="White" ForeColor="#333333" />
                          <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                          <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                          <RowStyle BackColor="White" ForeColor="#333333" />
                          <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                          <SortedAscendingCellStyle BackColor="#F7F7F7" />
                          <SortedAscendingHeaderStyle BackColor="#487575" />
                          <SortedDescendingCellStyle BackColor="#E5E5E5" />
                          <SortedDescendingHeaderStyle BackColor="#275353" />


                          <Columns>
                
                              <asp:TemplateField HeaderText="Prescribed By">
                                  <ItemTemplate>
                                     
                                      <asp:Label runat="server" Text='<%#Eval("PrescribedBy")%>' ></asp:Label>
                                      <asp:HiddenField ID="idHiddenField" runat="server" Value='<%#Eval("ImageId")%>'/>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Designation">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("Designation")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Action">
                                  <ItemTemplate>
                                     
                                      <asp:LinkButton ID="prescriptionShowLinkButton" runat="server" OnClick="prescriptionShowLinkButton_OnClick">Show</asp:LinkButton>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                             

                          </Columns>
                          

                      </asp:GridView>
                  </div>
                     
                  <div class="col-md-6 col-sm-6"></div>
                        </div>   
                    </div>
            
        
            <!-- Report -->
                 <div class="col-md-12 col-sm-12">
                     
                     <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                        
                     </div>
                         <!-- CONTACT FORM HERE -->
              <div class="wow fadeInUp" data-wow-delay="0.8s">

                  <div class="col-md-6 col-sm-6">
                      <label for="name">Patient Reports</label>
                      <asp:GridView ID="reportGridView" runat="server" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal" Width="100%" AutoGenerateColumns="False">
                          <FooterStyle BackColor="White" ForeColor="#333333" />
                          <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                          <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                          <RowStyle BackColor="White" ForeColor="#333333" />
                          <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                          <SortedAscendingCellStyle BackColor="#F7F7F7" />
                          <SortedAscendingHeaderStyle BackColor="#487575" />
                          <SortedDescendingCellStyle BackColor="#E5E5E5" />
                          <SortedDescendingHeaderStyle BackColor="#275353" />


                          <Columns>
                
                              <asp:TemplateField HeaderText="Report Name">
                                  <ItemTemplate>
                                      <asp:HiddenField ID="idHiddenField" runat="server" Value='<%#Eval("ImageId")%>'/>
                                      <asp:Label runat="server" Text='<%#Eval("ReportName")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                              

                              <asp:TemplateField HeaderText="Prescribed By">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("PrescribedBy")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Designation">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("Designation")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Action">
                                  <ItemTemplate>
                                     
                                      <asp:LinkButton ID="reportShowLinkButton" runat="server" OnClick="reportShowLinkButton_OnClick">Show</asp:LinkButton>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                             

                          </Columns>
                          

                      </asp:GridView>
                  </div>
                  <div class="col-md-6 col-sm-6"></div>
                            
                        </div>   
                    </div>
        </div>
          </div>

     </section>
          

      </form>

<!-- SCRIPTS -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>
