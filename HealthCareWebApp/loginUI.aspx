﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="loginUI.aspx.cs" Inherits="HealthCareWebApp.loginUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome To Health Care</title>


    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/tooplate-style.css">


</head>
<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

    <!-- PRE LOADER -->
    <section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>



     <!-- MENU -->
     <section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="loginUI.aspx" class="navbar-brand">P M <i class="fa fa-h-square"></i> System</a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#top" class="smoothScroll">Home</a></li>
                <li><a href="#about" class="smoothScroll">About Us</a></li>
                <li><a href="#team" class="smoothScroll">Team Member</a></li>
                
                <li><a href="MedicalTestInquiry.aspx" class="smoothScroll">Diagnostic-Center</a></li>
               
                <li class="appointment-btn"><a href="#appointment">Sign In / Sign Up</a></li>
            </ul>
        </div>

    </div>
</section>


     <!-- HOME -->
     <section id="home" class="slider" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                         <div class="owl-carousel owl-theme">
                              <div class="item item-first">
                                  <div class="caption">
                                      <div class="col-md-offset-1 col-md-10">
                                          <h3>type</h3>
                                          <h1>Welcome To Our Website</h1>
                                          <a href="#appointment" class="section-btn btn btn-default btn-blue smoothScroll">Create an Account To Get Services</a>
                                      </div>
                                  </div>
                              </div>

                              <div class="item item-second">
                                   <div class="caption">
                                        <div class="col-md-offset-1 col-md-10">
                                             <h3>Demo line Type Which u Want</h3>
                                             <h1>New Lifestyle</h1>
                                             <a href="#about" class="section-btn btn btn-default btn-gray smoothScroll">More About Our Project</a>
                                        </div>
                                   </div>
                              </div>

                              <div class="item item-third">
                                  
                                  <div class="caption">
                                      <div class="col-md-offset-1 col-md-10">
                                          <h3>Let's make your life happier</h3>
                                          <h1>Healthy Living</h1>
                                          <a href="#team" class="section-btn btn btn-default smoothScroll">Meet Our Team</a>
                                      </div>
                                  </div>

                                  
                              </div>
                         </div>

               </div>
          </div>
     </section>


     <!-- ABOUT -->
     <section id="about">
          <div class="container">
               <div class="row">

                    <div class="col-md-6 col-sm-6">
                         <div class="about-info">
                              <h2 class="wow fadeInUp" data-wow-delay="0.6s">Welcome to Your <i class="fa fa-h-square"></i>ealth Care</h2>
                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                   <p>Type</p>
                                   <p>type</p>
                              </div>
                              <figure class="profile wow fadeInUp" data-wow-delay="1s">
                                   <img src="images/author-image.jpg" class="img-responsive" alt="">
                                   <figcaption>
                                        <h3>name</h3>
                                        <p>type</p>
                                   </figcaption>
                              </figure>
                         </div>
                    </div>
                    
               </div>
          </div>
     </section>


     <!-- TEAM -->
     <section id="team" data-stellar-background-ratio="1">
          <div class="container">
               <div class="row">

                    <div class="col-md-6 col-sm-6">
                         <div class="about-info">
                              <h2 class="wow fadeInUp" data-wow-delay="0.1s">Our Team Members</h2>
                         </div>
                    </div>

                    <div class="clearfix"></div>

                  

                    <div class="col-md-4 col-sm-6">
                         <div class="team-thumb wow fadeInUp" data-wow-delay="0.4s">
                              <img src="images/team-image2.jpg" class="img-responsive" alt="">

                                   <div class="team-info">
                                        <h3>name</h3>
                                        <p>id</p>
                                        <div class="team-contact-info">
                                             <p><i class="fa fa-phone"></i> 01866666</p>
                                             <p><i class="fa fa-envelope-o"></i> <a href="#">mail id</a></p>
                                        </div>
                                        <ul class="social-icon">
                                             <li><a href="#" class="fa fa-facebook-square"></a></li>
                                             
                                        </ul>
                                   </div>

                         </div>
                    </div>
                   
                   <div class="col-md-4 col-sm-6">
                       <div class="team-thumb wow fadeInUp" data-wow-delay="0.4s">
                           <img src="images/team-image2.jpg" class="img-responsive" alt="">

                           <div class="team-info">
                               <h3>name</h3>
                               <p>id</p>
                               <div class="team-contact-info">
                                   <p><i class="fa fa-phone"></i> 01866666</p>
                                   <p><i class="fa fa-envelope-o"></i> <a href="#">mail id</a></p>
                               </div>
                               <ul class="social-icon">
                                   <li><a href="#" class="fa fa-facebook-square"></a></li>
                                             
                               </ul>
                           </div>

                       </div>
                   </div>
                   
                   <div class="col-md-4 col-sm-6">
                       <div class="team-thumb wow fadeInUp" data-wow-delay="0.4s">
                           <img src="images/team-image2.jpg" class="img-responsive" alt="">

                           <div class="team-info">
                               <h3>name</h3>
                               <p>id</p>
                               <div class="team-contact-info">
                                   <p><i class="fa fa-phone"></i> 01866666</p>
                                   <p><i class="fa fa-envelope-o"></i> <a href="#">mail id</a></p>
                               </div>
                               <ul class="social-icon">
                                   <li><a href="#" class="fa fa-facebook-square"></a></li>
                                             
                               </ul>
                           </div>

                       </div>
                   </div>

               
                    
               </div>
          </div>
     </section>


    <!-- Main Coding Html Side -->

    <form id="form1" runat="server">
        <section id="appointment" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">
            
            <div class="col-md-6 col-sm-6">
                <!-- CONTACT FORM HERE -->
                         

                <!-- SECTION TITLE -->
                <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                    <h2>Sign In</h2>
                </div>

                <div class="wow fadeInUp" data-wow-delay="0.8s">
                                 

                                  


                    <div class="col-md-12 col-sm-12">
                        <label for="email">Email</label>
                        <asp:TextBox ID="usernameTextbox" class="form-control" placeholder="Your Email" runat="server"></asp:TextBox>
                                       

                        <label for="telephone">Password</label>
                        <asp:TextBox ID="passwordTextBox" class="form-control" placeholder="Password" TextMode="Password"  runat="server"></asp:TextBox>
                                       
                                        
                                        
                    </div>
                                  
                    <div class="col-md-6 col-sm-6">
                        <asp:Label ID="signinLabel" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:Button ID="signinButton" class="form-control" runat="server" Text="Sign In" OnClick="signinButton_Click" />
                                     
                    </div>
                                  

                </div>
                     
            </div>

        <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h2>Sign Up</h2>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                  
                                  <div class="col-md-12 col-sm-12">
                                      <label for="email">Name</label>
                                      <asp:TextBox ID="nameTextBox" class="form-control" placeholder="Your Full Name" runat="server"></asp:TextBox>
                                      <label for="email">Email</label>
                                      <asp:TextBox ID="emailTextBox" class="form-control" placeholder="Your Email" runat="server"></asp:TextBox>
                                       

                                      <label for="telephone">Password</label>
                                      <asp:TextBox ID="passTextBox" class="form-control" placeholder="Password" TextMode="Password"  runat="server"></asp:TextBox>
                                       
                                     
                                  </div>
                                  
                                  <div class="col-md-6 col-sm-6">
                                      <label for="select">Select User Type</label> 
                                      <asp:DropDownList ID="usertypeDropDownList" class="form-control" runat="server">
                                          <asp:ListItem>Select User Type</asp:ListItem>
                                          <asp:ListItem>General</asp:ListItem>
                                          <asp:ListItem>Doctor</asp:ListItem>
                                      </asp:DropDownList>  
                                  </div>
                                      
                                  <div class="col-md-6 col-sm-6">
                                      
                                      <label for="telephone">Mobile</label>
                                      <asp:TextBox ID="mobileTextBox" class="form-control" placeholder="Mobile Number"  runat="server"></asp:TextBox>

                                  </div>
                                  

                                  <div class="col-md-6 col-sm-6">
                                      <asp:Label ID="signupLabel" runat="server" Text=""></asp:Label>
                                  </div>
                                  <div class="col-md-6 col-sm-6">
                                      <asp:Button ID="signupButton" class="form-control" runat="server" Text="Sign Up" OnClick="signupButton_Click" />
                                     
                                  </div>

                              </div>

        </div>
        
         

               </div>
          </div>

     </section>
        
        

    </form>

<!-- SCRIPTS -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>



</body>
</html>
