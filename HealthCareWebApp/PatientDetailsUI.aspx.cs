﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HealthCareWebApp.BLL;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp
{
    public partial class PatientDetailsUI : System.Web.UI.Page
    {
        UserManager aUserManager = new UserManager();
        Users aUser = new Users();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null && Session["PatientId"]!= null)
            {
                int patientId = Convert.ToInt32(Session["PatientId"]);
                aUser = aUserManager.GetUsersById(patientId);

                patientNameTextBox.Text = aUser.Name;
                mobileTextbox.Text = aUser.Mobile;

                //Load All History Data of Patient

                patientHistoryGridView.DataSource = aUserManager.GetAllPatientData(Convert.ToInt32(Session["PatientId"]));
                patientHistoryGridView.DataBind();

                //Load All Prescription of Patient

                prescriptionGridView.DataSource = aUserManager.GetAllPrescriptionOfPatient(Convert.ToInt32(Session["PatientId"]));
                prescriptionGridView.DataBind();

                //Load All Report of Patient

                reportGridView.DataSource = aUserManager.GetAllReportOfPatient(Convert.ToInt32(Session["PatientId"]));
                reportGridView.DataBind();
            }
            else
            {
                Response.Redirect("loginUI.aspx");
            }

        }


        protected void prescriptionShowLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell = (DataControlFieldCell)updateLinkButton.Parent;
            GridViewRow row = (GridViewRow)cell.Parent;
            HiddenField idHiddenField = (HiddenField)row.FindControl("idHiddenField");
            int imageId = Convert.ToInt32(idHiddenField.Value);
            Response.Redirect("DoctorPrecsriptionImageUI.aspx?imageId=" + imageId);
        }

        protected void reportShowLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell = (DataControlFieldCell)updateLinkButton.Parent;
            GridViewRow row = (GridViewRow)cell.Parent;
            HiddenField idHiddenField = (HiddenField)row.FindControl("idHiddenField");
            int imageId = Convert.ToInt32(idHiddenField.Value);
            Response.Redirect("DoctorReportImageUI.aspx?imageId=" + imageId);
        }

    }
}