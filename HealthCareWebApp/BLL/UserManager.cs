﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HealthCareWebApp.DAL.Gateway;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp.BLL
{
    public class UserManager
    {
        UserGateway aUserGateway = new UserGateway();

        public string SaveUsers(Users aUsers)
        {
            if (aUserGateway.UsersExist(aUsers.Email))
            {
                return "Already Exist User";
            }
            else
            {
                int rowAffect = aUserGateway.SaveUsers(aUsers);
                if (rowAffect > 0)
                {
                    return "Save Successful";
                }
                else
                {
                    return "Operation Fail";
                }
            }
        }

        public Users GetUsers(string userEmail)
        {
            Users aUsers = new Users();
            if (aUserGateway.UsersExist(userEmail))
            {
                aUsers = aUserGateway.GetUsers(userEmail);
                return aUsers;
            }
            else
            {
                return aUsers = null;
            }
        }


        public Users GetUsersById(int userId)
        {
            return aUserGateway.GetUsersById(userId);
        }

        public string SavePatientData(PatientData aPatientData)
        {
            if (aPatientData.BloodPressure == "" && aPatientData.Diabetes == "")
            {
                return "Fill All The TextBox Properly";
            }
            else
            {
                int rowAffect = aUserGateway.SavePatientData(aPatientData);
                if (rowAffect > 0)
                {
                    return "Save Successful";
                }
                else
                {
                    return "Operation Fail";
                }
            }
        }

        public List<PatientData> GetAllPatientData(int userId)
        {
            List<PatientData> aPatientDataList = new List<PatientData>();
            aPatientDataList = aUserGateway.GetAllPatientData(userId);
            return aPatientDataList;
        }


        public string SavePrescription(Prescription aPrescription)
        {
            if (aPrescription.PrescribedBy != "" && aPrescription.Designation != "Select a Designation")
            {
                int rowAffect = aUserGateway.SavePrescription(aPrescription);
                if (rowAffect > 0)
                {
                    return "Save Successful";
                }
                else
                {
                    return "Operation Fail";
                }
            }
            else
            {
                return "Fill all TextBox & Select Properly";
            }
        }


        public List<Prescription> GetAllPrescriptionOfPatient(int userId)
        {
            List<Prescription> aPatientPrescriptionsList = new List<Prescription>();
            aPatientPrescriptionsList = aUserGateway.GetAllPrescriptionOfPatient(userId);
            return aPatientPrescriptionsList;
        }


        public Prescription GetPrescriptionImage(int imageId)
        {
            return aUserGateway.GetPrescriptionImage(imageId);
        }


        public string SaveReport(Report aReport)
        {
            if (aReport.PrescribedBy != "" && aReport.Designation != "Select a Designation" && aReport.ReportName != "")
            {
                int rowAffect = aUserGateway.SaveReport(aReport);
                if (rowAffect > 0)
                {
                    return "Save Successful";
                }
                else
                {
                    return "Operation Fail";
                }
            }
            else
            {
                return "Fill all TextBox & Select Properly";
            }
        }


        public List<Report> GetAllReportOfPatient(int userId)
        {
            List<Report> aPatientReportsList = new List<Report>();
            aPatientReportsList = aUserGateway.GetAllReportOfPatient(userId);
            return aPatientReportsList;
        }


        public Report GetReportImage(int imageId)
        {
            return aUserGateway.GetReportImage(imageId);
        }


        public List<Users> GetAllDoctors(String userType)
        {
            return aUserGateway.GetAllDoctors(userType);
        }


        public string SaveDoctorInPermissionTable(Permission aPermission)
        {
            if (aUserGateway.DoctorExistInPermissionTable(aPermission))
            {
                return "Doctor Already Approved";
            }
            else
            {
                int rowAffect = aUserGateway.SaveDoctorInPermissionTable(aPermission);
                if (rowAffect > 0)
                {
                    return "Save Successful";
                }
                else
                {
                    return "Operation Fail";
                }
            }
        }


        public List<Permission> GetAllPermittedDoctors(int patientId)
        {
            return aUserGateway.GetAllPermittedDoctors(patientId);
        }


        public string DeleteDoctorFromPermission(int patientId, int doctorId)
        {
            int rowAffect = aUserGateway.DeleteDoctorFromPermission(patientId, doctorId);
            if (rowAffect > 0)
            {
                return "Save Successful";
            }
            else
            {
                return "Operation Fail";
            }
        }


        public List<Permission> GetAllPermittedPatient(int doctorId)
        {
            return aUserGateway.GetAllPermittedPatient(doctorId);
        }


        public string SaveDiagnosticCenter(DiagnosticCenter center)
        {
            if (aUserGateway.UsersExist(center.Email))
            {
                Users user = aUserGateway.GetUsers(center.Email);
                center.CenterId = user.UserId;
                center.CenterName = user.Name;

                if (aUserGateway.DiagnosticCenterExist(center.CenterId))
                {
                    return "Diagnostic Center Information Already Registered";
                }
                else
                {
                    int rowAffect = aUserGateway.SaveDiagnosticCenter(center);
                    if (rowAffect > 0)
                    {
                        return "Save Successful";
                    }
                    else
                    {
                        return "Operation Fail";
                    }
                }

            }
            else
            {
                return "This Email is Not Register For Any Diagnostic Center";
            }
        }

        public List<DiagnosticCenter> GetAllDiagnosticCenterData()
        {
            List<DiagnosticCenter> aDiagnosticCenterList = new List<DiagnosticCenter>();
            aDiagnosticCenterList = aUserGateway.GetAllDiagnosticCenterData();
            return aDiagnosticCenterList;
        }

        public string SaveMedicalTestData(MedicalTest test)
        {
            if (aUserGateway.TestExistByNameAndCenterId(test))
            {
                return "This Medical Test Info Already Exist";
            }
            else
            {
                int rowAffect = aUserGateway.SaveMedicalTest(test);
                if (rowAffect > 0)
                {
                    return "Save Successful";
                }
                else
                {
                    return "Operation Fail";
                }
            }
        }

        public object GetAllMedicalTestData(int centerId)
        {
            List<MedicalTest> aMedicalTestList = new List<MedicalTest>();
            aMedicalTestList = aUserGateway.GetAllMedicalTestData(centerId);
            return aMedicalTestList;
        }


        public object GetAllLocation(string city)
        {
            return aUserGateway.GetAllLocation(city);
        }

        public object GetAllCenterByLocation(string city, string location)
        {
            return aUserGateway.GetAllCenterByLocation(city, location);
        }
    }
}