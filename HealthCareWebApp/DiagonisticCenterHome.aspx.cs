﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HealthCareWebApp.BLL;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp
{
    public partial class DiagonisticCenterHome : System.Web.UI.Page
    {
        UserManager aUserManager = new UserManager();
        Users aUser = new Users();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                string email = Session["User"].ToString();
                aUser = aUserManager.GetUsers(email);

                //Load All History Data of Patient

                testGridView.DataSource = aUserManager.GetAllMedicalTestData(aUser.UserId);
                testGridView.DataBind();
            }
            else
            {
                Response.Redirect("loginUI.aspx");
            }
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            MedicalTest test = new MedicalTest();
            test.CenterId = aUser.UserId;
            test.TestName = nameTextbox.Text;
            test.TestPrice = priceTextbox.Text;
            test.TestTime = timeTextBox.Text;
            test.TestPrerequisite = prerequisiteTextBox.Text;

            string message = aUserManager.SaveMedicalTestData(test);

            if (message == "Save Successful")
            {
                saveLavel.Text = message;
                nameTextbox.Text = String.Empty;
                priceTextbox.Text = String.Empty;
                timeTextBox.Text = String.Empty;
                prerequisiteTextBox.Text = String.Empty;

                testGridView.DataSource = aUserManager.GetAllMedicalTestData(aUser.UserId);
                 testGridView.DataBind();
            }
            else
            {
                saveLavel.Text = message;
            }


        }
    }
}