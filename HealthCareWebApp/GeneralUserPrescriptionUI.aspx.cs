﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HealthCareWebApp.BLL;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp
{
    public partial class GeneralUserPrescriptionUI : System.Web.UI.Page
    {
        UserManager aUserManager=new UserManager();
        Users aUser=new Users();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                string email = Session["User"].ToString();
                aUser = aUserManager.GetUsers(email);

                //Load All Prescription of Patient

                prescriptionGridView.DataSource = aUserManager.GetAllPrescriptionOfPatient(aUser.UserId);
                prescriptionGridView.DataBind();
            }
            else
            {
                Response.Redirect("loginUI.aspx");
            }
        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            HttpPostedFile postedFile = imageFileUpload.PostedFile;
            string fileName = Path.GetFileName(postedFile.FileName);
            string fileExtention = Path.GetExtension(fileName);
            if (fileExtention.ToLower()==".jpg" || fileExtention.ToLower() == ".png" || fileExtention.ToLower() == ".jpeg")
            {
                Prescription aPrescription=new Prescription();
                aPrescription.PrescribedBy = prescribedByTextbox.Text;
                aPrescription.Designation = designationDropDownList.SelectedValue;
                aPrescription.UserId = aUser.UserId;

                Stream stream = postedFile.InputStream;
                BinaryReader binaryReader=new BinaryReader(stream);
                aPrescription.Image = binaryReader.ReadBytes((int) stream.Length);

                string message = aUserManager.SavePrescription(aPrescription);

                if (message== "Save Successful")
                {
                    saveLavel.Text = message;
                    prescribedByTextbox.Text=String.Empty;
                    imageFileUpload.Attributes.Clear();
                    designationDropDownList.ClearSelection();

                    prescriptionGridView.DataSource = aUserManager.GetAllPrescriptionOfPatient(aUser.UserId);
                    prescriptionGridView.DataBind();
                }
                else
                {
                    saveLavel.Text = message;
                }

            }
            else
            {
                saveLavel.Text = "Select Proper Image File (.jpg/.png)";
                saveLavel.ForeColor = System.Drawing.Color.Red;
            }

        }


        protected void showLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell = (DataControlFieldCell)updateLinkButton.Parent;
            GridViewRow row = (GridViewRow)cell.Parent;
            HiddenField idHiddenField = (HiddenField)row.FindControl("idHiddenField");
            int imageId = Convert.ToInt32(idHiddenField.Value);
            Response.Redirect("Image.aspx?imageId=" + imageId);
        }


    }
}