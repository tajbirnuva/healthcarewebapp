﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HealthCareWebApp.BLL;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp
{
    public partial class loginUI : System.Web.UI.Page
    {
        UserManager aUserManager = new UserManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["User"] = null;
        }

        protected void signupButton_Click(object sender, EventArgs e)
        {
            Users aUser=new Users();
            aUser.Name = nameTextBox.Text;
            aUser.Email = emailTextBox.Text;
            aUser.Password = passTextBox.Text;
            aUser.Mobile = mobileTextBox.Text;
            aUser.UserType = usertypeDropDownList.SelectedValue;

            string message = aUserManager.SaveUsers(aUser);
            if (message == "Already Exist User")
            {
                signupLabel.Text = message;
            }
            else if (message == "Operation Fail")
            {
                signupLabel.Text = message + "--Please Try Again";
            }
            else
            {
                if (aUser.UserType=="General")
                {
                    Session["User"] = aUser.Email;
                    Response.Redirect("GeneralUserHomeUI.aspx");
                }
                else
                {
                    Session["User"] = aUser.Email;
                    Response.Redirect("DoctorUserHomeUI.aspx");
                }
                
               
            }



        }

        protected void signinButton_Click(object sender, EventArgs e)
        {
            Users aUser = new Users();
            string userEmail = usernameTextbox.Text;
            string password = passwordTextBox.Text;

            aUser = aUserManager.GetUsers(userEmail);

            if (aUser != null)
            {
                if (aUser.Email == userEmail && aUser.Password == password)
                {
                    if (aUser.UserType == "General")
                    {
                        Session["User"] = aUser.Email;
                        Response.Redirect("GeneralUserHomeUI.aspx");
                    }
                    else if(aUser.UserType == "Doctor")
                    {
                        Session["User"] = aUser.Email;
                        Response.Redirect("DoctorUserHomeUI.aspx");
                    }
                    else if (aUser.UserType == "Admin")
                    {
                        Session["User"] = aUser.Email;
                        Response.Redirect("AdminHomeUI.aspx");
                    }
                    else
                    {
                        Session["User"] = aUser.Email;
                        Response.Redirect("DiagonisticCenterHome.aspx");
                    }
                }

            }
            else
            {
                signinLabel.Text = "User not Found";
            }

        }
    }
}