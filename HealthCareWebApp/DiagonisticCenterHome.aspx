﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DiagonisticCenterHome.aspx.cs" Inherits="HealthCareWebApp.DiagonisticCenterHome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Home</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/tooplate-style.css">


</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>



<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="loginUI.aspx" class="navbar-brand">P M <i class="fa fa-h-square"></i> System</a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="DiagonisticCenterHome.aspx" class="smoothScroll">Home</a></li>
              

                <li class="appointment-btn"><a href="loginUI.aspx">Log Out</a></li>
            </ul>
        </div>
    </div>
</section>



      <form id="form1" runat="server">
        <section id="appointment" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h2>Diagnostic  Center Test Info</h2>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                  
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">Test Name</label>
                                      <asp:TextBox ID="nameTextbox" class="form-control" placeholder="Type Test Name" runat="server"></asp:TextBox>

                                  </div>

                                  <div class="col-md-6 col-sm-6">
                                      <label for="email">Service Charge</label>
                                      <asp:TextBox ID="priceTextbox" class="form-control" placeholder="Type Service Charge (300/- TK)" runat="server"></asp:TextBox>

                                  </div>
                                  
                                  <div class="col-md-6 col-sm-6">
                                      <label for="email">Test Time</label>
                                      <asp:TextBox ID="timeTextBox" class="form-control" placeholder="Type Time (10.00am to 12.00pm)" runat="server"></asp:TextBox>

                                  </div>
                                  
                                  <div class="col-md-12 col-sm-12">
                                      <label for="email">Test Prerequisite</label>
                                      <asp:TextBox ID="prerequisiteTextBox" class="form-control" placeholder="Enter Test Prerequisite" runat="server"></asp:TextBox>

                                  </div>
                                  

                                  <div class="col-md-12 col-sm-12">
                                     
                                      <asp:Button ID="saveButton" class="form-control" runat="server" Text="Save" OnClick="saveButton_Click" />
                                     
                                  </div>

                                  <div class="col-md-12 col-sm-12">
                                      <asp:Label ID="saveLavel" runat="server" Text=""></asp:Label>
                                  </div>
                                  

                              </div>

        </div>
        
          <div class="col-md-12 col-sm-12">
              
                         <!-- CONTACT FORM HERE -->
              <div class="wow fadeInUp" data-wow-delay="0.8s">
                  <h4 class="text-center">Test Details</h4>

                  <div class="col-md-12 col-sm-12">
                      <asp:GridView ID="testGridView" runat="server" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal" Width="100%" AutoGenerateColumns="False">
                          <FooterStyle BackColor="White" ForeColor="#333333" />
                          <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                          <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                          <RowStyle BackColor="White" ForeColor="#333333" />
                          <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                          <SortedAscendingCellStyle BackColor="#F7F7F7" />
                          <SortedAscendingHeaderStyle BackColor="#487575" />
                          <SortedDescendingCellStyle BackColor="#E5E5E5" />
                          <SortedDescendingHeaderStyle BackColor="#275353" />


                          <Columns>
                
                              <asp:TemplateField HeaderText="Test Name">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("TestName")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Time">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("TestTime")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Price (Tk)">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("TestPrice")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Prerequisite">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("TestPrerequisite")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>

                          </Columns>
                          

                      </asp:GridView>
                  </div>
                            
                        </div>   
                    </div>

               </div>
          </div>

     </section>
      </form>

<!-- SCRIPTS -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>

</body>
</html>
