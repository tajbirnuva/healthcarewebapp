﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HealthCareWebApp.BLL;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp
{
    public partial class Image : System.Web.UI.Page
    {
        UserManager aUserManager=new UserManager();
        Prescription aPrescription=new Prescription();
        protected void Page_Load(object sender, EventArgs e)
        {
            int imageId = Convert.ToInt32(Request.QueryString["imageId"]);
            aPrescription = aUserManager.GetPrescriptionImage(imageId);

            prescribedByTextbox.Text = aPrescription.PrescribedBy;
            string prescriptionImage = Convert.ToBase64String(aPrescription.Image);

            prescriptionPic.ImageUrl = String.Format("data:image/jpg;base64,{0}", prescriptionImage);


        }
    }
}