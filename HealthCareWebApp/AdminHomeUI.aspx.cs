﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HealthCareWebApp.BLL;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp
{
    public partial class AdminHomeUI : System.Web.UI.Page
    {
        UserManager aUserManager = new UserManager();
        Users aUser=new Users();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                string email = Session["User"].ToString();
                aUser = aUserManager.GetUsers(email);
            }
            else
            {
                Response.Redirect("loginUI.aspx");
            }
        }

        protected void signupButton_Click(object sender, EventArgs e)
        {
            Users aUser = new Users();
            aUser.Name = nameTextBox.Text;
            aUser.Email = emailTextBox.Text;
            aUser.Password = passTextBox.Text;
            aUser.Mobile = mobileTextBox.Text;
            aUser.UserType = usertypeDropDownList.SelectedValue;

            string message = aUserManager.SaveUsers(aUser);
            if (message == "Already Exist User")
            {
                signupLabel.Text = message;
            }
            else if (message == "Operation Fail")
            {
                signupLabel.Text = message + "--Please Try Again";
            }
            else
            {
                signupLabel.Text = message;
                nameTextBox.Text=String.Empty;
                emailTextBox.Text=String.Empty;
                passTextBox.Text=String.Empty;
                mobileTextBox.Text=String.Empty;
                nameTextBox.Text=String.Empty;
                usertypeDropDownList.SelectedIndex=0;
            }



        }
    }
}