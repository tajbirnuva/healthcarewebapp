﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralUserReportUI.aspx.cs" Inherits="HealthCareWebApp.GeneralUserReportUI" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Medical Reports</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/tooplate-style.css">


</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>



<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="loginUI.aspx" class="navbar-brand">P M <i class="fa fa-h-square"></i> System</a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="GeneralUserHomeUI.aspx" class="smoothScroll">Home</a></li>
                <li><a href="GeneralUserPrescriptionUI.aspx" class="smoothScroll">Prescription</a></li>
                <li><a href="GeneralUserReportUI.aspx" class="smoothScroll">Reports</a></li>

                <li class="appointment-btn"><a href="GeneralUserAccountUI.aspx">Account</a></li>
            </ul>
        </div>
    </div>
</section>




    
      <form id="form1" runat="server">
        <section id="appointment" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h2>Report Details</h2>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                  
                                  <div class="col-md-6 col-sm-6">
                                      <label for="name">Prescribed By</label>
                                      <asp:TextBox ID="prescribedByTextbox" class="form-control" placeholder="Enter Your Doctor's Name" runat="server"></asp:TextBox>

                                  </div>

                                  <div class="col-md-6 col-sm-6">
                                      <label for="date">Doctor's Designation</label>
                                      <asp:DropDownList ID="designationDropDownList" class="form-control" runat="server">
                                          <asp:ListItem>Select a Designation</asp:ListItem>
                                          <asp:ListItem>Allergist/Immunologist</asp:ListItem>
                                          <asp:ListItem>Anesthesiologist</asp:ListItem>
                                          <asp:ListItem>Cardiologist</asp:ListItem>
                                          <asp:ListItem>Dermatologist</asp:ListItem>
                                          <asp:ListItem>Dental Surgeon</asp:ListItem>
                                          <asp:ListItem>Endocrinologist</asp:ListItem>
                                          <asp:ListItem>Gastroenterologist</asp:ListItem>
                                          <asp:ListItem>Medicine</asp:ListItem>
                                          <asp:ListItem>Neuro Medicine</asp:ListItem>
                                          <asp:ListItem>Neurologist</asp:ListItem>
                                          <asp:ListItem>Nephrologist</asp:ListItem>
                                          <asp:ListItem>Ophthalmologist</asp:ListItem>
                                          <asp:ListItem>Oncologist</asp:ListItem>
                                          <asp:ListItem>Otolaryngologist</asp:ListItem>
                                          <asp:ListItem>Orthopedist</asp:ListItem>
                                          <asp:ListItem>OB/GYN</asp:ListItem>
                                          <asp:ListItem>Psychiatrist</asp:ListItem>
                                          <asp:ListItem>Surgery</asp:ListItem>
                                          <asp:ListItem>Urologist</asp:ListItem>
                                      </asp:DropDownList>
                                     
                                  </div>
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">Report Name</label>
                                      <asp:TextBox ID="reportNameTextBox" class="form-control" placeholder="Enter Your Medical Report's/Test Name" runat="server"></asp:TextBox>
                                  </div>
                                  
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">File</label>
                                      <asp:FileUpload ID="imageFileUpload" class="form-control" runat="server" />
                                  </div>

                                  <div class="col-md-12 col-sm-12">
                                     
                                      <asp:Button ID="saveButton" class="form-control" runat="server" Text="Save" OnClick="saveButton_Click" />
                                     
                                  </div>

                                  <div class="col-md-12 col-sm-12">
                                      <asp:Label ID="saveLavel" runat="server" Text=""></asp:Label>
                                  </div>
                                  

                              </div>

        </div>
        
          <div class="col-md-6 col-sm-6">
                         <!-- CONTACT FORM HERE -->
              <div class="wow fadeInUp" data-wow-delay="0.8s">

                  <div class="col-md-12 col-sm-12">
                      <asp:GridView ID="reportGridView" runat="server" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal" Width="100%" AutoGenerateColumns="False">
                          <FooterStyle BackColor="White" ForeColor="#333333" />
                          <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                          <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                          <RowStyle BackColor="White" ForeColor="#333333" />
                          <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                          <SortedAscendingCellStyle BackColor="#F7F7F7" />
                          <SortedAscendingHeaderStyle BackColor="#487575" />
                          <SortedDescendingCellStyle BackColor="#E5E5E5" />
                          <SortedDescendingHeaderStyle BackColor="#275353" />


                          <Columns>
                
                              <asp:TemplateField HeaderText="Report Name">
                                  <ItemTemplate>
                                      <asp:HiddenField ID="idHiddenField" runat="server" Value='<%#Eval("ImageId")%>'/>
                                      <asp:Label runat="server" Text='<%#Eval("ReportName")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                              

                              <asp:TemplateField HeaderText="Prescribed By">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("PrescribedBy")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Designation">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("Designation")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Action">
                                  <ItemTemplate>
                                     
                                      <asp:LinkButton ID="showLinkButton" runat="server" OnClick="showLinkButton_OnClick">Show</asp:LinkButton>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                             

                          </Columns>
                          

                      </asp:GridView>
                  </div>
                            
                        </div>   
                    </div>

               </div>
          </div>

     </section>
      </form>

<!-- SCRIPTS -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>
