﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HealthCareWebApp.BLL;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp
{
    public partial class GeneralUserHomeUI : System.Web.UI.Page
    {

        UserManager aUserManager = new UserManager();
        Users aUser = new Users();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"]!=null)
            {
                string email = Session["User"].ToString();
                aUser=aUserManager.GetUsers(email);

                //Load All History Data of Patient
                
                    patientHistoryGridView.DataSource = aUserManager.GetAllPatientData(aUser.UserId);
                    patientHistoryGridView.DataBind();
            }
            else
            {
                Response.Redirect("loginUI.aspx");
            }

        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            PatientData aPatientData=new PatientData();

            aPatientData.UserId = aUser.UserId;
            aPatientData.BloodPressure = bpTextbox.Text;
            aPatientData.Diabetes = diabetesTextbox.Text;

            //Time and Date Taking
            DateTime utcTime = DateTime.UtcNow;
            TimeZoneInfo BdZone = TimeZoneInfo.FindSystemTimeZoneById("Bangladesh Standard Time");
            DateTime localDateTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, BdZone);

            aPatientData.Date = localDateTime.ToString("MMMM dd");
            aPatientData.Time = localDateTime.ToString("h:mm tt");

            string message = aUserManager.SavePatientData(aPatientData);

            if (message== "Save Successful")
            {
                saveLavel.Text = message;
                bpTextbox.Text=String.Empty;
                diabetesTextbox.Text=String.Empty;

                patientHistoryGridView.DataSource = aUserManager.GetAllPatientData(aUser.UserId);
                patientHistoryGridView.DataBind();
            }
            else
            {
                saveLavel.Text = message;
            }

        }
    }
}