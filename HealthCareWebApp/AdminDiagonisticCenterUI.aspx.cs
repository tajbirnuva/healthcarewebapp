﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HealthCareWebApp.BLL;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp
{
    public partial class AdminDiagonisticCenterUI : System.Web.UI.Page
    {
        UserManager aUserManager = new UserManager();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User"] != null)
            {
                string email = Session["User"].ToString();
               

                //Load All History Data of Patient

                centerGridView.DataSource = aUserManager.GetAllDiagnosticCenterData();
                centerGridView.DataBind();
            }
            else
            {
                Response.Redirect("loginUI.aspx");
            }
        }

        protected void permissionButton_Click(object sender, EventArgs e)
        {
            DiagnosticCenter center = new DiagnosticCenter();
            center.Email = emailTextBox.Text;
            center.Location = locationTextBox.Text;
            center.City = cityTextBox.Text;
            center.Mobile = mobileTextBox.Text;
            center.Opening = openTextBox.Text;
            center.Closing = closeTextBox.Text;
           
            string message = aUserManager.SaveDiagnosticCenter(center);
            
            if (message == "This Email is Not Register For Any Diagnostic Center")
            {
                saveLavel.Text = message;
            }
            else
            {
                saveLavel.Text = message;
                //Load All History Data of Patient

                centerGridView.DataSource = aUserManager.GetAllDiagnosticCenterData();
                centerGridView.DataBind();

                emailTextBox.Text = string.Empty;
                locationTextBox.Text=String.Empty;
                cityTextBox.Text=String.Empty;
            }
        }
    }
}