﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp.DAL.Gateway
{
    public class UserGateway : BassGateway
    {
        public bool UsersExist(string email)
        {
            string query = "SELECT * FROM Users WHERE Email=@Email";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@Email", email);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }


        public int SaveUsers(Users aUser)
        {
            string query = "INSERT INTO Users(Name,Password,UserType,Email,Mobile) VALUES (@Name,@Password,@UserType,@Email,@Mobile)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@Name", aUser.Name);
            Command.Parameters.AddWithValue("@Password", aUser.Password);
            Command.Parameters.AddWithValue("@UserType", aUser.UserType);
            Command.Parameters.AddWithValue("@Email", aUser.Email);
            Command.Parameters.AddWithValue("@Mobile", aUser.Mobile);


            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }


        public Users GetUsers(string userEmail)
        {
            string query = "SELECT * From Users WHERE Email=@Email";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@Email", userEmail);

            Connection.Open();
            Reader = Command.ExecuteReader();
            Users aUsers = new Users();
            Reader.Read();

            aUsers.UserId = Convert.ToInt32(Reader["UserId"]);
            aUsers.Name = Reader["Name"].ToString();
            aUsers.Password = Reader["Password"].ToString();
            aUsers.UserType = Reader["UserType"].ToString();
            aUsers.Email = Reader["Email"].ToString();
            aUsers.Mobile = Reader["Mobile"].ToString();

            Connection.Close();
            return aUsers;
        }


        public Users GetUsersById(int userId)
        {
            string query = "SELECT * From Users WHERE UserId=@UserId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@UserId", userId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            Users aUsers = new Users();
            Reader.Read();

            aUsers.UserId = Convert.ToInt32(Reader["UserId"]);
            aUsers.Name = Reader["Name"].ToString();
            aUsers.Password = Reader["Password"].ToString();
            aUsers.UserType = Reader["UserType"].ToString();
            aUsers.Email = Reader["Email"].ToString();
            aUsers.Mobile = Reader["Mobile"].ToString();

            Connection.Close();
            return aUsers;
        }


        public int SavePatientData(PatientData aPatientData)
        {
            string query = "INSERT INTO PatientData(UserId,Date,Time,BloodPressure,Diabetes) VALUES (@UserId,@Date,@Time,@BloodPressure,@Diabetes)";
            Command = new SqlCommand(query, Connection);
            
            Command.Parameters.AddWithValue("@UserId", aPatientData.UserId);
            Command.Parameters.AddWithValue("@Date", aPatientData.Date);
            Command.Parameters.AddWithValue("@Time", aPatientData.Time);
            Command.Parameters.AddWithValue("@BloodPressure", aPatientData.BloodPressure);
            Command.Parameters.AddWithValue("@Diabetes", aPatientData.Diabetes);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }


        public List<PatientData> GetAllPatientData(int userId)
        {
            string query = "SELECT * FROM PatientData WHERE UserId=@UserId ORDER BY Date";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@UserId", userId);
           
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<PatientData> aPatientDataList = new List<PatientData>();
            while (Reader.Read())
            {
                PatientData aPatientData=new PatientData();

                aPatientData.Date = Reader["Date"].ToString();
                aPatientData.Time = Reader["Time"].ToString();
                aPatientData.BloodPressure = Reader["BloodPressure"].ToString();
                aPatientData.Diabetes = Reader["Diabetes"].ToString();

                aPatientDataList.Add(aPatientData);
            }

            Connection.Close();

            return aPatientDataList;
        }


        public int SavePrescription(Prescription aPrescription)
        {
            string query = "INSERT INTO Prescription(Image,PrescribedBy,Designation,UserId) VALUES (@Image,@PrescribedBy,@Designation,@UserId)";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@Image", aPrescription.Image);
            Command.Parameters.AddWithValue("@PrescribedBy", aPrescription.PrescribedBy);
            Command.Parameters.AddWithValue("@Designation", aPrescription.Designation);
            Command.Parameters.AddWithValue("@UserId", aPrescription.UserId);
            

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }


        public List<Prescription> GetAllPrescriptionOfPatient(int userId)
        {
            string query = "SELECT * FROM Prescription WHERE UserId=@UserId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@UserId", userId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Prescription> aPatientPrescriptionsList = new List<Prescription>();
            while (Reader.Read())
            {
                Prescription aPrescription =new Prescription();

                aPrescription.ImageId = Convert.ToInt32(Reader["ImageId"]);
                aPrescription.PrescribedBy = Reader["PrescribedBy"].ToString();
                aPrescription.Designation = Reader["Designation"].ToString();
                

                aPatientPrescriptionsList.Add(aPrescription);
            }

            Connection.Close();

            return aPatientPrescriptionsList;
        }


        public Prescription GetPrescriptionImage(int imageId)
        {
            string query = "SELECT * From Prescription WHERE ImageId=@ImageId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@ImageId", imageId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            Prescription aPrescription=new Prescription();
            Reader.Read();

            
            aPrescription.PrescribedBy = Reader["PrescribedBy"].ToString();
            aPrescription.Image =(byte[]) Reader["Image"];
            

            Connection.Close();
            return aPrescription;
        }


        public int SaveReport(Report aReport)
        {
            string query = "INSERT INTO Report(ReportName,PrescribedBy,Designation,Image,UserId) VALUES (@ReportName,@PrescribedBy,@Designation,@Image,@UserId)";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@ReportName", aReport.ReportName);
            Command.Parameters.AddWithValue("@PrescribedBy", aReport.PrescribedBy);
            Command.Parameters.AddWithValue("@Designation", aReport.Designation);
            Command.Parameters.AddWithValue("@Image", aReport.Image);
            Command.Parameters.AddWithValue("@UserId", aReport.UserId);


            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }


        public List<Report> GetAllReportOfPatient(int userId)
        {
            string query = "SELECT * FROM Report WHERE UserId=@UserId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@UserId", userId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Report> aPatientReportsList = new List<Report>();
            while (Reader.Read())
            {
                Report aReport=new Report();

                aReport.ImageId = Convert.ToInt32(Reader["ImageId"]);
                aReport.ReportName = Reader["ReportName"].ToString();
                aReport.PrescribedBy = Reader["PrescribedBy"].ToString();
                aReport.Designation = Reader["Designation"].ToString();


                aPatientReportsList.Add(aReport);
            }

            Connection.Close();

            return aPatientReportsList;
        }


        public Report GetReportImage(int imageId)
        {
            string query = "SELECT * From Report WHERE ImageId=@ImageId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@ImageId", imageId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            Report aReport=new Report();
            Reader.Read();


            aReport.PrescribedBy = Reader["PrescribedBy"].ToString();
            aReport.ReportName = Reader["ReportName"].ToString();
            aReport.Image = (byte[])Reader["Image"];


            Connection.Close();
            return aReport;
        }


        public List<Users> GetAllDoctors(String userType)
        {
            string query = "SELECT * FROM Users WHERE UserType=@UserType";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@UserType", userType);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Users> aDoctorList = new List<Users>();
            while (Reader.Read())
            {
                Users aUser=new Users();

                aUser.UserId = Convert.ToInt32(Reader["UserId"]);
                aUser.Name = Reader["Name"].ToString();
                
                aDoctorList.Add(aUser);
            }

            Connection.Close();

            return aDoctorList;
        }


        public bool DoctorExistInPermissionTable(Permission aPermission)
        {
            string query = "SELECT * FROM Permission WHERE PatientId=@PatientId AND DoctorId=@DoctorId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@PatientId", aPermission.PatientId);
            Command.Parameters.AddWithValue("@DoctorId", aPermission.DoctorId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }


        public int SaveDoctorInPermissionTable(Permission aPermission)
        {
            string query = "INSERT INTO Permission(PatientId,PatientName,DoctorId,DoctorName) VALUES (@PatientId,@PatientName,@DoctorId,@DoctorName)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@PatientId", aPermission.PatientId);
            Command.Parameters.AddWithValue("@PatientName", aPermission.PatientName);
            Command.Parameters.AddWithValue("@DoctorId", aPermission.DoctorId);
            Command.Parameters.AddWithValue("@DoctorName", aPermission.DoctorName);


            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }


        public List<Permission> GetAllPermittedDoctors(int patientId)
        {
            string query = "SELECT * FROM Permission WHERE PatientId=@PatientId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@PatientId", patientId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Permission> aPermittedDoctorList = new List<Permission>();
            while (Reader.Read())
            {
                Permission aPermission=new Permission();

                aPermission.PatientId = Convert.ToInt32(Reader["PatientId"]);
                aPermission.DoctorId = Convert.ToInt32(Reader["DoctorId"]);
                aPermission.PatientName = Reader["PatientName"].ToString();
                aPermission.DoctorName = Reader["DoctorName"].ToString();

                aPermittedDoctorList.Add(aPermission);
            }

            Connection.Close();

            return aPermittedDoctorList;
        }


        public int DeleteDoctorFromPermission(int patientId, int doctorId)
        {
            string query = "DELETE FROM Permission WHERE PatientId=@PatientId AND DoctorId=@DoctorId";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@PatientId", patientId);
            Command.Parameters.AddWithValue("@DoctorId",doctorId);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }


        public List<Permission> GetAllPermittedPatient(int doctorId)
        {
            string query = "SELECT * FROM Permission WHERE DoctorId=@DoctorId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@DoctorId", doctorId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Permission> aPermittedPatientList = new List<Permission>();
            while (Reader.Read())
            {
                Permission aPermission = new Permission();

                aPermission.PatientId = Convert.ToInt32(Reader["PatientId"]);
                aPermission.DoctorId = Convert.ToInt32(Reader["DoctorId"]);
                aPermission.PatientName = Reader["PatientName"].ToString();
                aPermission.DoctorName = Reader["DoctorName"].ToString();

                aPermittedPatientList.Add(aPermission);
            }

            Connection.Close();

            return aPermittedPatientList;
        }


        public int SaveDiagnosticCenter(DiagnosticCenter center)
        {
            string query = "INSERT INTO DiagnosticCenter(CenterId,CenterName,Location,City,Email,Mobile,Opening,Closing) VALUES (@CenterId,@CenterName,@Location,@City,@Email,@,Mobile,@Opening,@Closing)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@CenterId", center.CenterId);
            Command.Parameters.AddWithValue("@CenterName", center.CenterName);
            Command.Parameters.AddWithValue("@Location", center.Location);
            Command.Parameters.AddWithValue("@City", center.City);
            Command.Parameters.AddWithValue("@Email", center.Email);
            Command.Parameters.AddWithValue("@Mobile", center.Mobile);
            Command.Parameters.AddWithValue("@Opening", center.Opening);
            Command.Parameters.AddWithValue("@Closing", center.Closing);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public bool DiagnosticCenterExist(int centerId)
        {
            string query = "SELECT * FROM DiagnosticCenter WHERE CenterId=@CenterId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@CenterId", centerId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }


        public List<DiagnosticCenter> GetAllDiagnosticCenterData()
        {
            string query = "SELECT * FROM DiagnosticCenter";
            Command = new SqlCommand(query, Connection);
            
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<DiagnosticCenter> aDiagnosticCenterList = new List<DiagnosticCenter>();
            while (Reader.Read())
            {
                DiagnosticCenter center = new DiagnosticCenter();

                center.CenterName = Reader["CenterName"].ToString();
                center.Email = Reader["Email"].ToString();
                center.Location = Reader["Location"].ToString();
                center.City = Reader["City"].ToString();
                center.Mobile = Reader["Mobile"].ToString();
                center.Opening = Reader["Opening"].ToString();
                center.Closing = Reader["Closing"].ToString();

                aDiagnosticCenterList.Add(center);
            }

            Connection.Close();

            return aDiagnosticCenterList;
        }

        public bool TestExistByNameAndCenterId(MedicalTest test)
        {
            string query = "SELECT * FROM MedicalTest WHERE CenterId=@CenterId AND TestName=@TestName";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@CenterId", test.CenterId);
            Command.Parameters.AddWithValue("@TestName", test.TestName);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }

        public int SaveMedicalTest(MedicalTest test)
        {
            string query = "INSERT INTO MedicalTest(CenterId,TestName,TestPrice,TestTime,TestPrerequisite) VALUES (@CenterId,@TestName,@TestPrice,@TestTime,@TestPrerequisite)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@CenterId", test.CenterId);
            Command.Parameters.AddWithValue("@TestName", test.TestName);
            Command.Parameters.AddWithValue("@TestPrice", test.TestPrice);
            Command.Parameters.AddWithValue("@TestTime", test.TestTime);
            Command.Parameters.AddWithValue("@TestPrerequisite", test.TestPrerequisite);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public List<MedicalTest> GetAllMedicalTestData(int centerId)
        {
            string query = "SELECT * FROM MedicalTest WHERE CenterId=@CenterId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@CenterId", centerId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<MedicalTest> aMedicalTestList = new List<MedicalTest>();

            while (Reader.Read())
            {
                MedicalTest test = new MedicalTest();

                test.TestName = Reader["TestName"].ToString();
                test.TestTime = Reader["TestTime"].ToString();
                test.TestPrice = Reader["TestPrice"].ToString();
                test.TestPrerequisite = Reader["TestPrerequisite"].ToString();

                aMedicalTestList.Add(test);
            }

            Connection.Close();

            return aMedicalTestList;
        }

        public List<Place> GetAllCity()
        {

            string query = "SELECT * FROM Place";
            Command = new SqlCommand(query, Connection);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Place> placeList = new List<Place>();

            while (Reader.Read())
            {
                Place aPlace = new Place();

                aPlace.Id =Convert.ToInt32(Reader["Id"]);
                aPlace.City = Reader["City"].ToString();
                aPlace.Location = Reader["Location"].ToString();


                placeList.Add(aPlace);
            }

            Connection.Close();

            return placeList;
        }

        public object GetAllLocation(string city)
        {
            string query = "SELECT * FROM Place WHERE City=@City";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@City", city);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Place> placeList = new List<Place>();

            while (Reader.Read())
            {
                Place aPlace = new Place();

                aPlace.Location = Reader["Location"].ToString();


                placeList.Add(aPlace);
            }

            Connection.Close();

            return placeList;
        }

        public object GetAllCenterByLocation(string city, string location)
        {
            string query = "SELECT * FROM DiagnosticCenter WHERE CITY=@City AND Location=@Location";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@City", city);
            Command.Parameters.AddWithValue("@Location", location);


            Connection.Open();
            Reader = Command.ExecuteReader();
            List<DiagnosticCenter> aDiagnosticCenterList = new List<DiagnosticCenter>();
            while (Reader.Read())
            {
                DiagnosticCenter center = new DiagnosticCenter();

                center.CenterName = Reader["CenterName"].ToString();
                center.Email = Reader["Email"].ToString();
                center.Location = Reader["Location"].ToString();
                center.City = Reader["City"].ToString();
                center.Mobile = Reader["Mobile"].ToString();
                center.Opening = Reader["Opening"].ToString();
                center.Closing = Reader["Closing"].ToString();

                aDiagnosticCenterList.Add(center);
            }

            Connection.Close();

            return aDiagnosticCenterList;
        }
    }
}