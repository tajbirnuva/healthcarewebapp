﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace HealthCareWebApp.DAL.Gateway
{
    public class BassGateway
    {
        public SqlConnection Connection { get; set; }
        public SqlCommand Command { get; set; }
        public SqlDataReader Reader { get; set; }




        public BassGateway()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["HealthCareConnectionString"].ConnectionString;

            Connection = new SqlConnection(connectionString);
        }




    }
}