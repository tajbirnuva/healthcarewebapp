﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareWebApp.DAL.Model
{
    public class PatientData
    {
        public int PatientDataId { get; set; }
        public int UserId { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string BloodPressure { get; set; }
        public string Diabetes { get; set; }
    }
}