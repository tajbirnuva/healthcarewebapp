﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareWebApp.DAL.Model
{
    public class DoctorDesignation
    {
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
    }
}