﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareWebApp.DAL.Model
{
    public class Place
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string Location { get; set; }

    }
}