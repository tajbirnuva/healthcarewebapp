﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareWebApp.DAL.Model
{
    public class MedicalTest
    {
        public string TestName { get; set; }
        public string TestPrice { get; set; }
        public string TestTime { get; set; }
        public string TestPrerequisite { get; set; }
        public int CenterId { get; set; }

    }
}