﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareWebApp.DAL.Model
{
    public class DiagnosticCenter
    {
        public string Email { get; set; }
        public int CenterId { get; set; }
        public string CenterName { get; set; }
        public string Location { get; set; }
        public string City { get; set; }
        public string Mobile { get; set; }
        public string Opening { get; set; }
        public string Closing { get; set; }
    }
}