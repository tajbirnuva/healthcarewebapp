﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareWebApp.DAL.Model
{
    public class Users
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
    }
}