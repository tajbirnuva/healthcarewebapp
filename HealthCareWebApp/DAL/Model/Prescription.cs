﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthCareWebApp.DAL.Model
{
    public class Prescription
    {

        public int ImageId { get; set; }
        public byte[] Image { get; set; }
        public int UserId { get; set; }
        public string PrescribedBy { get; set; }
        public string Designation { get; set; }
        
    }
}