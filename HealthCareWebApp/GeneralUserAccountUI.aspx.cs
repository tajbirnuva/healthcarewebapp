﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HealthCareWebApp.BLL;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp
{
    public partial class GeneralUserAccountUI : System.Web.UI.Page
    {
        UserManager aUserManager = new UserManager();
        Users aUser = new Users();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                string email = Session["User"].ToString();
                aUser = aUserManager.GetUsers(email);
                if (!IsPostBack)
                {
                    doctorDropDownList.DataSource = aUserManager.GetAllDoctors("Doctor");
                    doctorDropDownList.DataTextField = "Name";
                    doctorDropDownList.DataValueField = "UserId";
                    doctorDropDownList.DataBind();
                    doctorDropDownList.Items.Insert(0, "Select Your Desire Doctor");

                    //Load All Permitted Doctor
                    selectedDoctortGridView.DataSource = aUserManager.GetAllPermittedDoctors(aUser.UserId);
                    selectedDoctortGridView.DataBind();
                }

                
            }
            else
            {
                Response.Redirect("loginUI.aspx");
            }

        }

        protected void doctorDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (doctorDropDownList.SelectedValue == "Select Your Desire Doctor")
            {
                saveLavel.Text = "Select Doctor's Name";
                mobileTextbox.Text = String.Empty;
                
            }
            else
            {
                int userId = Convert.ToInt32(doctorDropDownList.SelectedValue);

                Users aUsers = aUserManager.GetUsersById(userId);
                mobileTextbox.Text = aUsers.Mobile.ToString();

                saveLavel.ForeColor = System.Drawing.Color.ForestGreen;
                saveLavel.Text = "Check Mobile Number To Sure Right Doctor";
            }
        }

        protected void permissionButton_Click(object sender, EventArgs e)
        {
            if (doctorDropDownList.SelectedValue== "Select Your Desire Doctor")
            {
                saveLavel.Text = "Select Doctor's Name";
                mobileTextbox.Text = String.Empty;
            }
            else
            {
                Permission aPermission = new Permission();

                aPermission.PatientId = aUser.UserId;
                aPermission.PatientName = aUser.Name;

                aPermission.DoctorId = Convert.ToInt32(doctorDropDownList.SelectedValue);
                Users aUsers = aUserManager.GetUsersById(aPermission.DoctorId);
                aPermission.DoctorName = aUsers.Name;

                string message = aUserManager.SaveDoctorInPermissionTable(aPermission);
                if (message == "Doctor Already Approved")
                {
                    saveLavel.Text = message;
                }
                else if (message == "Operation Fail")
                {
                    saveLavel.Text = message + "--Please Try Again";
                }
                else
                {
                    saveLavel.Text = message;
                    mobileTextbox.Text = String.Empty;
                    doctorDropDownList.SelectedIndex = 0;

                    selectedDoctortGridView.DataSource = aUserManager.GetAllPermittedDoctors(aUser.UserId);
                    selectedDoctortGridView.DataBind();

                }
            }
            

        }



        protected void deleteLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell = (DataControlFieldCell)updateLinkButton.Parent;
            GridViewRow row = (GridViewRow)cell.Parent;
            HiddenField idHiddenField = (HiddenField)row.FindControl("idHiddenField");
            int doctorId = Convert.ToInt32(idHiddenField.Value);
            int patientId = aUser.UserId;

            string message = aUserManager.DeleteDoctorFromPermission(patientId, doctorId);
            if (message== "Save Successful")
            {
                saveLavel.Text = "Delete Successfully";
                selectedDoctortGridView.DataSource = aUserManager.GetAllPermittedDoctors(aUser.UserId);
                selectedDoctortGridView.DataBind();
            }
            else
            {
                saveLavel.Text = "Operation Fail";
            }

        }

    }
}