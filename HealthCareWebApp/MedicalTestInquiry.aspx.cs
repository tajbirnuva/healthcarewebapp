﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HealthCareWebApp.BLL;

namespace HealthCareWebApp
{
    public partial class MedicalTestInquiry : System.Web.UI.Page
    {

        UserManager aUserManager = new UserManager();

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void cityDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cityDropDownList.SelectedValue == "-- Select a City--")
            {
                messageLevel.Text = "Select a City";
                locationDropDownList.Items.Clear();
                locationDropDownList.Items.Insert(0, "-- Select a Location (Unable) --");
            }
            else
            {
                locationDropDownList.DataSource = aUserManager.GetAllLocation(cityDropDownList.SelectedValue);
                locationDropDownList.DataTextField = "Location";
                locationDropDownList.DataValueField = "Location";
                locationDropDownList.DataBind();
                messageLevel.Text=String.Empty;
                if (locationDropDownList.Items.Count == 0)
                {
                    locationDropDownList.Items.Insert(0, "-- There is no  Diagnostic Center In This Locality --");

                }
                else
                {
                    locationDropDownList.Items.Insert(0, "-- Select a Location From " + cityDropDownList.SelectedValue + " City --");
                }

            }
        }

        protected void searchButton_Click(object sender, EventArgs e)
        {
            if (cityDropDownList.SelectedValue == "-- Select a City--" || locationDropDownList.SelectedValue == "-- Select a Location From " + cityDropDownList.SelectedValue + " City --")
            {
                messageLevel.Text = "Select Properly";
            }
            else
            {
                messageLevel.Text = String.Empty;
                //Load All Diagnostic Center Details

                centerGridView.DataSource = aUserManager.GetAllCenterByLocation(cityDropDownList.SelectedValue,locationDropDownList.SelectedValue);
                centerGridView.DataBind();
            }
        }


        protected void showLinkButton_OnClick(object sender, EventArgs e)
        {
            
        }

    }
}