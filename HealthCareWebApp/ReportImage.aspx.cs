﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HealthCareWebApp.BLL;
using HealthCareWebApp.DAL.Model;

namespace HealthCareWebApp
{
    public partial class ReportImage : System.Web.UI.Page
    {
        UserManager aUserManager = new UserManager();
        Report aReport=new Report();
        protected void Page_Load(object sender, EventArgs e)
        {
            int imageId = Convert.ToInt32(Request.QueryString["imageId"]);
            aReport = aUserManager.GetReportImage(imageId);

            reportTextBox.Text = aReport.ReportName;
            prescribedByTextbox.Text = aReport.PrescribedBy;


            string reportImage = Convert.ToBase64String(aReport.Image);
            reportPic.ImageUrl = String.Format("data:image/jpg;base64,{0}", reportImage);

        }
    }
}